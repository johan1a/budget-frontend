import React from "react";
import { Home } from "./Home";
import "main/css/MainPanel.css";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import { RootState } from "../store/index";
import Login from "./Login";
import { Upload } from "./Upload";
import { Transactions } from "./Transactions";
import { authService } from "main/services/authService";

const mapStateToProps = (state: RootState) => ({
  isLoggedIn: authService.isLoggedIn()
});

const mapDispatchToProps = {
};

const connector = connect(mapStateToProps, mapDispatchToProps);

type Props = ConnectedProps<typeof connector> & {};

const MainPanel: React.FC<Props> = (props) => {

    return (
      <div id="main-panel">
        <Router>
          <div>
            <Header isLoggedIn={props.isLoggedIn} />
            <div id="content">
              <Switch>
                <Route path="/about">
                  <About />
                </Route>
                <Route path="/login">
                  <Login />
                </Route>
                <Route path="/transactions">
                  <Transactions />
                </Route>
                <Route path="/upload">
                  <Upload />
                </Route>
                <Route path="/">
                  <Home />
                </Route>
              </Switch>
            </div>
          </div>
        </Router>
      </div>
    );
}

function Header(props: Props) {
  return (
    <header>
      <nav className="navbar-expand-md navbar-dark bg-dark">
        <div className="navbar-collapse">
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link className="nav-link" to="/">
                Home
              </Link>
            </li>
            {props.isLoggedIn ? (
              <li className="nav-item">
                <Link className="nav-link" to="/transactions">
                  Transactions
                </Link>
              </li>
            ) : null}
            {props.isLoggedIn ? (
              <li className="nav-item">
                <Link className="nav-link" to="/upload">
                  Upload
                </Link>
              </li>
            ) : null}
            <li className="nav-item">
              <Link className="nav-link" to="/about">
                About
              </Link>
            </li>
            {props.isLoggedIn ? null : (
              <li className="nav-item">
                <Link className="nav-link" to="/login">
                  Log in
                </Link>
              </li>
            )}
          </ul>
        </div>
      </nav>
    </header>
  );
}
export default connector(MainPanel);

function About() {
  return <h2>About</h2>;
}
