import React from "react";
import MainPanel from "./MainPanel";
import "main/css/App.css";
import "bootstrap/dist/css/bootstrap.min.css";

export const App: React.FC = (props) => {
  return (
    <div className="wrapper">
      <a
        rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
      >
        {" "}
      </a>
      <MainPanel></MainPanel>
    </div>
  );
};
