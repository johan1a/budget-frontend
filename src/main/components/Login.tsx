import React from "react";
import { Redirect } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import "main/css/Login.css";
import Cookies from "js-cookie";
import { connect, ConnectedProps } from "react-redux";
import { RootState } from "../store/index";
import { LoginAction } from "../store/auth/actions";
import { JwtToken } from "../store/auth/types";
import { authService } from "../services/authService";

const mapState = (state: RootState) => ({
  jwtToken: state.auth.jwtToken,
  isLoggedIn: authService.isLoggedIn()
});

const mapDispatch = {
  setToken: (p: JwtToken) => LoginAction(p)
};

const connector = connect(mapState, mapDispatch);

type Props = ConnectedProps<typeof connector> & {};

const Login: React.FC<Props> = props => {

  return (props.isLoggedIn) ? (
    <Redirect to="/transactions" />
  ) : (
    <div>
      <div id="content">
        <Form className="form-signin" onSubmit={handleSubmit}>
          <Form.Group controlId="formEmail">
            <Form.Label>Email</Form.Label>
            <Form.Control type="username" placeholder="Enter username" />
          </Form.Group>
          <Form.Group controlId="formPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" />
          </Form.Group>
          <Button variant="primary" type="submit">
            Sign in
          </Button>
        </Form>
      </div>
    </div>
  );

  async function handleSubmit(
    e: React.FormEvent<HTMLFormElement>
  ): Promise<void> {
    e.preventDefault();
    console.log("submitting");

    const usernameInput = document.getElementById(
      "formEmail"
    ) as HTMLInputElement;
    const username = usernameInput.value;

    const passwordInput = document.getElementById(
      "formPassword"
    ) as HTMLInputElement;
    const password = passwordInput.value;

    await submitForm(username, password);
  }

  async function submitForm(
    username: string,
    password: string
  ): Promise<boolean> {
    try {
      console.log("submitForm");
      console.log(props);
      const response = await fetch("/api/auth/login/", {
        method: "post",
        headers: new Headers({
          "Content-Type": "application/json",
          Accept: "application/json"
        }),
        body: JSON.stringify({
          username: username,
          password: password
        })
      });
      if (response.ok) {
        const data = await response.clone().json();
        const token = data["token"];
        Cookies.set("jwt_token", token);
        const jwtToken = authService.getJwtToken();
        props.setToken(jwtToken);
      }
      return response.ok;
    } catch (ex) {
      return false;
    }
  }
};
export default connector(Login);
