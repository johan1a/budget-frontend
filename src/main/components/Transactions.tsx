import React, { Component } from "react";
import { authService } from "main/services/authService";
import "main/css/Transactions.css";
import DataTable from "react-data-table-component";
import moment from "moment";

interface Transaction {
  id: string;
  currency: string;
  amount: number;
  date: string;
  text: string;
}

interface TransactionsState {
  transactions: Array<Transaction>;
  loading: boolean;
  error: boolean;
}

const COLUMN_DATE = "date";
const COLUMN_CURRENCY = "currency";
const COLUMN_TEXT = "text";
const COLUMN_AMOUNT = "amount";

const DATE_FORMAT = "DD-MM-YYYY";

function compareDates(date1: string, date2: string): number {
  const d1 = moment(date1, DATE_FORMAT);
  const d2 = moment(date2, DATE_FORMAT);
  let res = 0;
  if (d1 === d2) {
    res = 0;
  } else if (d1 > d2) {
    res = 1;
  } else {
    res = -1;
  }
  return res;
}

function sortColumn(
  rows: Array<Transaction>,
  column: string,
  direction: string
): Transaction[] {
  switch (column) {
    case COLUMN_DATE: {
      if (direction === "asc") {
        rows.sort((a, b) => compareDates(a.date, b.date));
      } else {
        rows.sort((a, b) => compareDates(b.date, a.date));
      }
      break;
    }
    case COLUMN_TEXT: {
      rows.sort((a, b) => a.text.localeCompare(b.text));
      break;
    }
    case COLUMN_CURRENCY: {
      rows.sort((a, b) => a.currency.localeCompare(b.currency));
      break;
    }
    case COLUMN_AMOUNT: {
      rows.sort((a, b) => a.amount - b.amount);
      break;
    }
  }
  if (column !== COLUMN_DATE && direction === "desc") {
    rows.reverse();
  }
  return rows;
}

const columns = [
  {
    name: "Date",
    selector: "date",
    sortable: true
  },
  {
    name: "Text",
    selector: "text",
    sortable: true
  },
  {
    name: "Amount",
    selector: "amount",
    sortable: true
  },
  {
    name: "Currency",
    selector: "currency",
    sortable: true
  }
];

export class Transactions extends Component {
  state: TransactionsState = {
    transactions: [],
    loading: true,
    error: false
  };

  async componentDidMount() {
    const token = authService.getToken();
    const response = await fetch(
      `/api/finance/users/${authService.getUserId()}/transactions/`,
      {
        headers: new Headers({
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        })
      }
    );
    try {
      const data = await response.json();
      this.setState({
        transactions: data.map((d: Transaction) => {
          return {
            date: d.date,
            currency: d.currency,
            text: d.text,
            amount: d.amount / 100
          };
        }),
        loading: false,
        error: false
      });
    } catch (error) {
      console.error("error:");
      console.error(error);
    }
  }

  title(): String {
    return "Transactions (" + this.state.transactions.length + " total)";
  }

  render() {
    return (
      <div id="transactions-container">
        <DataTable
          title={this.title()}
          columns={columns}
          data={this.state.transactions}
          sortFunction={sortColumn}
        />
      </div>
    );
  }
}
