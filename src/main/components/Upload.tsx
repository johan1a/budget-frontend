import React, { Component } from "react";
import { authService } from "main/services/authService";

interface UploadState {
  loading: boolean;
  error: boolean;
  file: string;
}

export class Upload extends Component {
  state: UploadState = {
    loading: true,
    error: false,
    file: ""
  };

  constructor(props: any) {
    super(props);
    this.uploadFile = this.uploadFile.bind(this);
    this.selectFile = this.selectFile.bind(this);
  }

  componentDidMount() {}

  uploadFile(event: React.MouseEvent<HTMLButtonElement, MouseEvent>) {
    const token = authService.getToken();
    const data = new FormData();
    data.append("file", this.state.file);
    fetch(
      `http://localhost:3000/api/finance/users/${authService.getUserId()}/transactionFiles/`,
      {
        method: "post",
        headers: new Headers({
          Authorization: `Bearer ${token}`
        }),
        body: data
      }
    );
    console.log("uploaded");
  }

  selectFile(event: React.ChangeEvent<HTMLInputElement>) {
    if (event !== null && event.target !== null) {
      const target = event.target;
      const files = target.files;
      if (files) {
        const file = files[0];
        console.log(file);
        this.setState({ loading: false, error: false, file: file });
      }
    }
  }

  render() {
    return (
      <div>
        <div>Upload transactions</div>
        <input type="file" name="file" onChange={this.selectFile} />

        <button type="button" className="block" onClick={this.uploadFile}>
          Upload
        </button>
      </div>
    );
  }
}
