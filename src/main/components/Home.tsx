import React from "react";
import "main/css/Home.css";

export const Home: React.FC = props => {
  return (
    <nav className="navbar navbar-light text-center">
      <div className="vertical-center">
        <h1> Hey</h1>
        <p>Something something </p>
        <a
          className="App-link"
          href="https://traeskfindr.com"
          target="_blank"
          rel="noopener noreferrer"
        >
          traeskfindr.com
        </a>
      </div>
    </nav>
  );
};
