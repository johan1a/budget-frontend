
export interface JwtToken {
  user?: JwtUser;
  exp: string;
}

export interface JwtUser {
  id: string;
  username: string;
  created_at: string;
}

export interface AuthState {
  jwtToken: JwtToken | null;
}

export const LOGIN = "LOGIN";
export const LOGOUT = "LOGOUT";

export interface LoginAction {
  type: typeof LOGIN;
  payload: JwtToken;
}

interface LogoutAction {
  type: typeof LOGOUT;
  meta: {
    timestamp: number;
  };
}

export type LoginTypes = LoginAction | LogoutAction;
