import { JwtToken, LOGIN, LOGOUT, LoginTypes } from './types'

// TypeScript infers that this function is returning LoginAction
export function LoginAction(newJwtToken: JwtToken): LoginTypes {
  return {
    type: LOGIN,
    payload: newJwtToken
  }
}

// TypeScript infers that this function is returning LogoutAction
export function LogoutAction(timestamp: number): LoginTypes {
  return {
    type: LOGOUT,
    meta: {
      timestamp
    }
  }
}
