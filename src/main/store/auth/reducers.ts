import { AuthState, LoginAction, LoginTypes, LOGIN, LOGOUT } from "./types";

const initialState: AuthState = {
  jwtToken: null
};

export function authReducer(
  state = initialState,
  action: LoginTypes
): AuthState {
  switch (action.type) {
    case LOGIN:
      return {
        jwtToken: (action as LoginAction).payload
      };
    case LOGOUT:
      return {
        jwtToken: null
      };
    default:
      return state;
  }
}
