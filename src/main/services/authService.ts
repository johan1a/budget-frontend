import Cookies from "js-cookie";
import jwt_decode from "jwt-decode";
import { JwtToken } from "../store/auth/types";

class AuthService {

  isLoggedIn(): boolean {
    console.log("authService isLoggedIn()");
    const token = Cookies.get("jwt_token");
    if (token) {
      const decoded: JwtToken = jwt_decode(token);
      console.log(decoded);
      console.log(decoded.user !== undefined);
      return decoded.user !== undefined;
    } else {
      return false;
    }
  }

  getJwtToken(): JwtToken {
    const token = Cookies.get("jwt_token");
    const decoded: JwtToken = jwt_decode(token!);
    return decoded;
  }

  getUserId(): String {
    const token = Cookies.get("jwt_token");
    if (token) {
      const decoded: JwtToken = jwt_decode(token);
      if (decoded) {
        console.log(decoded);
        return `${decoded.user!.id}`;
      } else {
        return "";
      }
    } else {
      return "";
    }
  }

  getToken(): string {
    const token = Cookies.get("jwt_token");
    return token ? token : "";
  }
}

export const authService = new AuthService();
