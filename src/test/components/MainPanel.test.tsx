import React from "react";
import MainPanel from "main/components/MainPanel";
import { mount } from "enzyme";
import { rootReducer } from "./../../main/store/index";
import { createStore } from 'redux';
import { Provider } from 'react-redux';

const store = createStore(rootReducer, {});

describe("MainPanel", () => {
  it("renders without crashing", () => {
    const wrapper = mount(
      <Provider store={store}>
        <MainPanel />
      </Provider>
    );
    const links = wrapper.find("a");

    expect(links.at(0).text()).toEqual("Home");
    expect(links.at(0).prop("href")).toEqual("/");

    expect(links.at(1).text()).toEqual("About");
    expect(links.at(1).prop("href")).toEqual("/about");

    expect(links.at(2).text()).toEqual("Log in");
    expect(links.at(2).prop("href")).toEqual("/login");
  });
});
