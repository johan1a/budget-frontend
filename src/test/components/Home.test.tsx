import React from "react";
import { Home } from "main/components/Home";
import { shallow } from "enzyme";

describe("Home", () => {
  it("renders welcome message", () => {
    const wrapper = shallow(<Home />);
    expect(wrapper.text()).toContain("Hey");
  });
});
